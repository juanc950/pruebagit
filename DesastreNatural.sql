USE [DesastreNatural]
GO
/****** Object:  Table [dbo].[Zona]    Script Date: 30/10/2016 23:58:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Zona](
	[Id_Zona] [int] IDENTITY(1,1) NOT NULL,
	[Usuario] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
	[Fecha] [date] NOT NULL,
	[IncentivoTotal] [float] NOT NULL,
 CONSTRAINT [PK_Zona] PRIMARY KEY CLUSTERED 
(
	[Id_Zona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Zona] ON 

INSERT [dbo].[Zona] ([Id_Zona], [Usuario], [Descripcion], [Fecha], [IncentivoTotal]) VALUES (1, N'ndoasndo', N'nioasdnsioa', CAST(0xDB3A0B00 AS Date), 1000)
INSERT [dbo].[Zona] ([Id_Zona], [Usuario], [Descripcion], [Fecha], [IncentivoTotal]) VALUES (2, N'nhpfn', N'iohno', CAST(0xFA3A0B00 AS Date), 5000)
INSERT [dbo].[Zona] ([Id_Zona], [Usuario], [Descripcion], [Fecha], [IncentivoTotal]) VALUES (4, N'niofnpo', N'ndsoadin', CAST(0xDC3A0B00 AS Date), 6000)
INSERT [dbo].[Zona] ([Id_Zona], [Usuario], [Descripcion], [Fecha], [IncentivoTotal]) VALUES (9, N'qqq', N'qq', CAST(0x003C0B00 AS Date), 200)
INSERT [dbo].[Zona] ([Id_Zona], [Usuario], [Descripcion], [Fecha], [IncentivoTotal]) VALUES (10, N'qq', N'qq', CAST(0xED3B0B00 AS Date), 100)
INSERT [dbo].[Zona] ([Id_Zona], [Usuario], [Descripcion], [Fecha], [IncentivoTotal]) VALUES (11, N'niofnpo', N'fdsf', CAST(0xED3B0B00 AS Date), 1500)
INSERT [dbo].[Zona] ([Id_Zona], [Usuario], [Descripcion], [Fecha], [IncentivoTotal]) VALUES (12, N'nbkonoas', N'nfpasnf', CAST(0x013C0B00 AS Date), 600)
SET IDENTITY_INSERT [dbo].[Zona] OFF
